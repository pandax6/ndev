import * as React from 'react'
import styled from 'styled-components'
import Alert from "@kiwicom/orbit-components/lib/Alert";

const Footer = (props) => {
	return (
		<div>
			<footer className="fat2, footer">
				<p> Copyright 2020 &copy; niledevs<br />
					<a className="fat2, highlightme2" href="/privacy">privacy</a>
					<a className="fat2, highlightme2" href="/tos">terms</a></p>
			</footer>
		</div >
	)
}


export default Footer;