import * as React from "react";
import NavigationBar from "@kiwicom/orbit-components/lib/NavigationBar";
import LinkList from "@kiwicom/orbit-components/lib/LinkList";
import TextLink from "@kiwicom/orbit-components/lib/TextLink";
import Link from 'next/link'


const Navbar = () => {
	return (
		<div>
			<h1 className='highlightme'> niledevs</h1>

			<li>

				<Link href="/#">
					<a className='hvr-border-fade'>home</a>
				</Link>
			</li>

			<Link href="/#contact">
				<a className='hvr-border-fade'>contact</a>
			</Link>
			{/*
			<li>
				<Link href="/privacy">
					<a className='fat'>privacy</a>
				</Link>
			</li>
			 */}
		</div>
	)
}

export default Navbar;