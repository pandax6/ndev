import styled from 'styled-components'
import Alert from "@kiwicom/orbit-components/lib/Alert";

const Head = (props) => {
	return (
		<div className="app">

			<Head>
				<title>NileDevs</title>

				<link
					href="https://fonts.googleapis.com/css?family=Roboto:400,400i,500,500i,700"
					rel="stylesheet"
				/>
			</Head>

		</div>
	)
}


export default Head;