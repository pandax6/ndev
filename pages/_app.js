
import * as React from 'react'
import App from 'next/app'
import Head from 'next/head'
import '../style/styles.css'
import { DefaultSeo } from 'next-seo'
import { getTokens } from '@kiwicom/orbit-components'
import { ThemeProvider, createGlobalStyle } from 'styled-components'
import Footer from '../Components/Footer'

import SEO from '../next-seo.config'
import Navbar from '../Components/Navbar'



const tokens = getTokens()

/* <DefaultSeo {...SEO} /> */
/*
const GlobalStyle = createGlobalStyle`
  body {
	display: flex;
	font-eight: 500;
    width: 100vw;
    height: 100vh;
    margin: 0 auto;
    background-color: ${({ theme }) => theme.orbit.paletteCloudLight};
  }
`
*/

const theme = {
	colors: {
		primary: '#0070f3',
	},
}

export default class MyApp extends App {
	render() {
		const { Component, pageProps } = this.props
		return (

			<>

				<Head>
					<title>NileDevs - E-commerce and SEO solutions</title>
					<meta name="viewport" content="width=device-width, initial-scale=1.0" />
					{/* Global site tag (gtag.js) - Google Analytics */}
					<script async src="https://www.googletagmanager.com/gtag/js?id=UA-173932182-1" />
					<link rel="favicon" href="../favicon.ico" />

					<script
						dangerouslySetInnerHTML={{
							__html: `<!-- Global site tag (gtag.js) - Google Analytics -->
              <script async src="https://www.googletagmanager.com/gtag/js?id=UA-173932182-1"></script>
       <script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-173932182-1');
</script>
<!-- Yandex.Metrika counter -->
<script type="text/javascript" >
   (function(m,e,t,r,i,k,a){m[i]=m[i]||function(){(m[i].a=m[i].a||[]).push(arguments)};
   m[i].l=1*new Date();k=e.createElement(t),a=e.getElementsByTagName(t)[0],k.async=1,k.src=r,a.parentNode.insertBefore(k,a)})
   (window, document, "script", "https://mc.yandex.ru/metrika/tag.js", "ym");

   ym(65974477, "init", {
        clickmap:true,
        trackLinks:true,
        accurateTrackBounce:true
   });
</script>
<noscript><div><img src="https://mc.yandex.ru/watch/65974477" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->

              `,
						}}
					></script>





				</Head>
				<Navbar />
				<DefaultSeo {...SEO} />
				<Component {...pageProps} />
				<Footer />
			</>

		)
	}
}