import * as React from 'react'
import styled from 'styled-components'
import Alert from "@kiwicom/orbit-components/lib/Alert";
const Button = styled.button`
  cursor: pointer;

  background: transparent;
  border-radius: 3px;
  border: 2px solid #3366ff;
  color: #3366ff;
  margin: 0.5em 1em;
  padding: 0.25em 1em;

  ${props => props.primary && css`
    background: #3366ff;
    color: white;
  `}
`

const Info = (props) => {
	return (
		<div className='container'>
			<div className="row">
				<div className="column">
					<div className="card">
						<h3>Web Development</h3>
						<p>Turn-key solutions for web application development are our core service.</p>
						<Button>contact</Button>
					</div>
				</div>

				<div className="column">
					<div className="card">
						<h3>E-Commerce</h3>
						<p>Modern e-commerce solutions for your business.</p>
						<Button>contact</Button>
					</div>
				</div>

				<div className="column">
					<div className="card">
						<h3>Search Engine Optimization</h3>
						<p>SEO Helps your business be noticed in the online world.</p>
						<Button>contact</Button>
					</div>
				</div>
			</div>
		</div>
	)
}


export default Info;
