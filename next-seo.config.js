
export default {
	openGraph: {
		type: 'website',
		locale: 'en_IE',
		site_name: 'NileDevs',
	},
	twitter: {
		handle: '@niledevs',
		site: 'https://twitter.com/niledevs',
		cardType: 'summary_large_image',
	},
}